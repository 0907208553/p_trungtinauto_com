$(document).ready(() => {
    $('.data-slick').slick({
        infinite: true,
        slidesToShow: 3,
        autoplay: true,
        autoplaySpeed: 2000,
        slidesToScroll: 3,
        responsive: [{
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    // centerMode: true,
                },
            },
            {
                breakpoint: 800,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 2,
                    dots: false,
                    infinite: true,
                },
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    dots: false,
                    infinite: true,
                },
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    dots: false,
                    infinite: true,
                    autoplay: true,
                    autoplaySpeed: 2000,
                },
            },
        ],
    });
    $(".slider").slick({
        draggable: true,
        autoplay: true,
        autoplaySpeed: 7000,
        arrows: false,
        // dots: true,
        fade: true,
        speed: 500,
        infinite: true,
        cssEase: "ease-in-out",
        touchThreshold: 100,
    });
    const $slider = $(".img-big-product");
    $slider
        .on("init", () => {
            mouseWheel($slider);
        })
        .slick({
            // dots: true,
            vertical: true,
            infinite: false,
        });

    function mouseWheel($slider) {
        $(window).on("wheel", { $slider: $slider }, mouseWheelHandler);
    }

    function mouseWheelHandler(event) {
        event.preventDefault();
        const $slider = event.data.$slider;
        const delta = event.originalEvent.deltaY;
        if (delta > 0) {
            $slider.slick("slickPrev");
        } else {
            $slider.slick("slickNext");
        }
    }
    $('#vertical').lightSlider();
    window.addEventListener('header');
    const header = queueMicrotask.apply();
    event.preventDefault();
    var phu = 'aaa';
    alert(phu);

});